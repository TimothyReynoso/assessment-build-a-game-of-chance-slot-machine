//Slot machine
const playbuttonDiv = document.querySelector('#playButton');
const min = Math.ceil(1);
const max = Math.floor(4);
const boxesDiv = document.querySelector('#containerforboxes');//not needed right now
let box1Div = document.querySelector('#box1');    
let box2Div = document.querySelector('#box2');
let box3Div = document.querySelector('#box3');
let resultDiv = document.querySelector('#result');

let playSlotmachine = function(){
//randomizing 1-3 
    let slot1 = Math.floor(Math.random()*(max-min))+min;
    let slot2 = Math.floor(Math.random()*(max-min))+min;
    let slot3 = Math.floor(Math.random()*(max-min))+min;
    //sending slot1 result to its div
    box1Div.textContent=slot1;
    //sending slot2 result to its div
    box2Div.textContent=slot2;
    //sending slot3 result to its div
    box3Div.textContent=slot3;

    if (slot3===3&&slot2===3&&slot1==3){
        resultDiv.textContent="You Won!!";
        resultDiv.style.color = 'orange';
        resultDiv.style.background = 'purple';
        box1Div.style.width = '100px';
        box1Div.style.height = '100px';
        box2Div.style.width = '100px';
        box2Div.style.height = '100px';
        box3Div.style.width = '100px';
        box3Div.style.height = '100px';
    }else{
        resultDiv.textContent="Try Again";
        resultDiv.style.color = 'black';
        resultDiv.style.background = 'rgb(182, 253, 112)';
        box1Div.style.width = '70px';
        box1Div.style.height = '70px';
        box2Div.style.width = '70px';
        box2Div.style.height = '70px';
        box3Div.style.width = '70px';
        box3Div.style.height = '70px';
    }
}
playbuttonDiv.addEventListener("click", playSlotmachine);